# hulk-shop

The tech stack for this project is VueJS, with Axios for API and a JSON Server for mock backend

## Project setup
1. Setup the current project directory (npm and axios installation)
```
npm install
npm install axios
```
2. Setup JSON server (first installation)
```
npm install -g json-server 
```
3. Create a db.json file with two fields: a products field (containing all the items listed in the products.json file), and an empty items field. Sample is provided for reference. (db.json). Preferably in a separate folder.

### Compile and hot-reload for development
1. Start the JSON server in the folder with db.json using the following command:
```
json-server --watch db.json
```
Addendum: To verify that server is online, go to http://localhost:3000. You should see an empty {items} field. 

2. Run the hulk-shop app
```
npm run serve
``` 
3. Go to http://localhost:8080 to check it out!

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Personal experience with the project
Part 1: I had fun making this project! It was quite a satisfying challenge. The only problem I could not resolve was fetching the data with the provided link, as the response kept returning empty. So I created my own product catalogue variable as a substitution, using the assets provided in the design specifications. Apart from that, there were no other roadblocks in the dev process.

It took me approximately four days to complete this project (two weekends, two workdays). Could've probably done it sooner, but I gotta go to office. :)

Except for the non-responsive API call, the project is complete with all major design points.

Part 2: There are certain things I was unable to finish in time with the new JSON data. 
    1. The first issue I faced was in the selection of a size and color combination for a particular item. If the size/color combination is present in the products list, there is no problem. However, when the combination is unavailable, the check function behaves unexpectedly due to a limitation in the Vue ecosystem. I was able to find a workaround to an extent, but certain cases still trigger the bug. I was therefore unable to complete the styling for the size selection boxes, as the bugs remain unresolved.
    2. The second issue I came across was the sorting of the list according to the drop-down list options. Things like Title, Product-Type, Product Vendor and so on require a field to enter the keyword with which to sort the products list, or a submenu containing all the keywords present in the list. I saw no provisions for the same in the design template I was provided.
    3. Pagination, or restricting the number of items in the page to a certain number. Was in the process of implementing it, but ran out of time. As a result, you'll find certain functions in the code which do nothing, that were meant for the pagination feature. Kindly ignore them. :)
